#include "..\api\util\util.hpp"
#include "..\api\ini_reader\ini_reader.hpp"
#include "..\api\common.hpp"

#include "..\api\http\http.hpp"
#include "..\api\google\google.hpp"
#include "..\api\google\drive\drive.hpp"

std::vector<std::string> headers { "Content-Type: multipart/related; boundary=foo_bar_baz" };
std::string post_data = R"(--foo_bar_baz
Content-Type: application/json; charset=UTF-8

{
"name": "testupload.txt"
}

--foo_bar_baz
Content-Type: plain/text

This is in the file
--foo_bar_baz--)";

// multipart upload txt file test
int main( ) {
	const auto googlez = std::make_unique< google >( "example", "example", "example" );

	const auto files = std::make_unique< drive >( googlez->token( ) );;
	files->upload( );

	for ( auto& line : files->data( ) ) {
		printf( "%s\n", line.c_str( ) );
	}

	system( "pause" );
	return 0;
}

/*int main( ) {
	//	construct our file
	const auto config = std::make_unique< ini_reader >( utility::current_directory( ), "config.ini" );

	printf( "looking for config..." );
	{
		//	ensure file exists
		if ( !config->exists( ) ) {
			printf( "failed to find config." );

			system( "pause" );
			return 0;
		}

		printf( "config found.\n" );
	}

	printf( "dumping contents..." );
	{
		config->dump( );
		printf( "dump complete.\n" );
	}

	printf( "would you like to list all variables? (y/n)" );
	{
		//	wait for user input and respond accordingly.
		std::string input;
		std::cin >> input;

		if ( input.find( 'y' ) != std::string::npos ) {
			config->read( );
			printf( "complete.\n" );
		}
	}

	printf( "please enter the variable you would like to retrieve." );
	{
		//	wait for user input and find first variable that contains input.
		std::string to_read;
		std::cin >> to_read;

		config->read( to_read );

		printf( "success.\n" );
	}

	system( "pause" );
	return 0;
}*/