#include "drive.hpp"

#include "..\google.hpp"

#include "..\..\common.hpp"
#include "..\..\util\util.hpp"

void drive::upload( ) {
	std::stringstream temp_stream;
	temp_stream << "Authorization: Bearer " << _authentication_token;
	std::vector<std::string> headers { "Content-Type: multipart/related; boundary=foo_bar_baz", temp_stream.str( ).c_str( ) };
	std::string post_data = R"(--foo_bar_baz
Content-Type: application/json; charset=UTF-8

{
"name": "testupload.txt"
}

--foo_bar_baz
Content-Type: plain/text

This is in the file
--foo_bar_baz--)";
	const auto page = std::make_unique< http >( "www.googleapis.com", "/upload/drive/v3/files?uploadType=multipart", http_send_type::post );

	page->headers( headers );
	page->send_data( post_data );
	page->send( );

	_debug = { std::to_string( page->status_code( ) ), page->get( http_get_type::response_body ) };

	// only call cleanup if using once or when done using it
	page->cleanup( );
}