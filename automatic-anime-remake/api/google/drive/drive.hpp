/*
*	google.hpp
*	- google api using http wrapper
*/

#pragma once
#include <string>
#include <vector>

#include "..\..\http\http.hpp"

// derived class requires my google api
class drive {
	std::string _authentication_token { };
	std::vector< std::string > _debug { };

public:
	drive( std::string token ) : _authentication_token( token ) { }

	std::vector< std::string > data( ) { return _debug; }

	void upload( );
};
