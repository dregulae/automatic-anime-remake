/*
*	google.hpp
*	- google api using http wrapper
*/

#pragma once
#include <string>
#include <vector>

#include "gauth.hpp"

#include "..\http\http.hpp"

// derived class requires my http wrapper
class google : public gauth {
public:
	google( std::string client_id = "", std::string client_secret = "", std::string refresh_token = "", const std::string &user_agent = "AAR", bool secure = 1 ) : gauth( client_id, client_secret, refresh_token ) { }

	std::string token( ) { return _authentication_token; }
};
