/*
*	gauth.hpp
*	- google authentication details for api
*/

#pragma once
#include <string>

class gauth {
	void create_authentication( );

protected:
	std::string _refresh_token { };
	std::string _client_id { };
	std::string _client_secret { };
	std::string _authentication_token { };

public:
	gauth( std::string client_id = "", std::string client_secret = "", std::string refresh_token = "" ) : _refresh_token( refresh_token ), _client_id( client_id ), _client_secret( client_secret ) { create_authentication( ); }
};
