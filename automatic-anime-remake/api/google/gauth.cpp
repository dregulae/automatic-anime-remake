#include "gauth.hpp"

#include "..\common.hpp"

#include "..\util\util.hpp"
#include "..\http\http.hpp"

void gauth::create_authentication( ) {
	std::stringstream temp_stream;
	temp_stream << "/oauth2/v4/token?client_secret=" << _client_secret << "&grant_type=refresh_token&refresh_token=" << _refresh_token << "&client_id=" << _client_id;

	const auto token = std::make_unique< http >( "www.googleapis.com", temp_stream.str( ).c_str( ), http_send_type::post );

	token->send( );

	auto lines = utility::splitter( " ", token->get( http_get_type::response_body ) );

	for (auto& line : lines ) {
		if ( line.find( "ya29." ) != std::string::npos ) {
			line.erase( std::remove( line.begin( ), line.end( ), '"' ), line.end( ) );
			_authentication_token = line;
			break;
		}
	}

	token->cleanup( );
}