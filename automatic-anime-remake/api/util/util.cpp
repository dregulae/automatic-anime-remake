#include "util.hpp"
#include "..\common.hpp"

#include <fstream>
#include <sstream>

#include <iterator>
#include <regex>

/*
 *	get_current_directory
 *	  - returns the directory the application was ran from.
 */
std::string utility::current_directory( ) {
	return std::filesystem::current_path( ).generic_string( );
}

std::string utility::file_all( std::string directory, std::string file_name ) {
	std::stringstream temp;
	temp << directory << "\\" << file_name;

	std::ifstream temp_file( temp.str( ) );
	return std::string( ( std::istreambuf_iterator<char>( temp_file ) ), std::istreambuf_iterator<char>( ) );
}

std::vector<std::string> utility::file_single( std::string directory, std::string file_name ) {
	std::vector<std::string> temp_deque;
	std::stringstream contents = std::stringstream( file_all( directory, file_name ) );

	return std::vector<std::string>( std::istream_iterator<std::string>( ( std::istream_iterator<std::string> ) contents ), std::istream_iterator< std::string >( ) );
}

std::vector<std::string> utility::splitter( std::string in_pattern, std::string content ) {
	std::string temp_content = content;
	std::vector<std::string> split_content;

	const std::regex pattern( in_pattern );
	copy( std::sregex_token_iterator( temp_content.begin( ), temp_content.end( ), pattern, -1 ),
		std::sregex_token_iterator( ), back_inserter( split_content ) );
	return split_content; 
} 