/*
 *	ini_reader.hpp
 *	  - dynamic ini reader that reads from file and stores
 */

#pragma once
#include <sstream>
#include <string>
#include <vector>

enum comments {
	nothing,
	regular,
	side
};

class ini_reader {
	std::string _current_directory { };
	std::string _file_name { };
	std::string _file_path { };

	comments comment( std::string ) const;

public:
	std::vector< std::pair< std::string, std::string > > _file_contents;

	ini_reader( const std::string &current_directory, const std::string &file_name ) : _current_directory( current_directory ), _file_name( file_name ) {
		std::stringstream temp;
		temp << _current_directory << "\\" << _file_name;

		_file_path = temp.str( );
	}

	bool exists( ) const;
	void dump( );

	void read( );
	void read( const std::string &variable );

	std::string value( const std::string &variable ) const;
};