/*
*	http.hpp
*	- http wrapper for microsoft wininet
*/

#pragma once
#include <string>
#include <vector>
#include <array>

enum http_get_type {
	request_headers,
	request_body,
	response_headers,
	response_body
};

enum http_send_type {
	post,
	put,
	patch,
	_delete,
	head,
	options
};

class http {
protected:
	std::vector< std::string > _headers { };
	std::string _send_data { };

	std::string _user_agent { };

	std::string _host { };
	std::string _post { };

	unsigned long _status_code = 0;

	void* _internet;
	void* _connect;
	void* _request;

	std::array<std::string, http_send_type::options + 1> http_send_type_char {
		"POST",
		"PUT",
		"PATCH",
		"DELETE",
		"HEAD",
		"OPTIONS"
	};

public:
	http( ) { };
	http( const std::string &host, const std::string &post, http_send_type httpsendtype, const std::string &user_agent = "AAR", bool secure = 1 );

	void headers( std::vector< std::string > );

	std::string get( http_get_type );
	int status_code( ) { return _status_code; }

	void send_data( std::string );
	void send( );

	void cleanup( );
};