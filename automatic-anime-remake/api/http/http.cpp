#include "http.hpp"

#include "..\common.hpp"

#include <WinInet.h>
#pragma comment (lib, "Wininet.lib")

http::http( const std::string &host, const std::string &post, http_send_type httpsendtype, const std::string &user_agent, bool secure ) : _host( host ), _post( post ), _user_agent( user_agent ) {
	_internet = InternetOpenA( _user_agent.c_str( ), INTERNET_OPEN_TYPE_PRECONFIG, nullptr, nullptr, 0 );
	if ( !_internet ) {
		printf( "InternetOpen failed\n" );
		return;
	}

	_connect = InternetConnectA( _internet, _host.c_str( ), secure ? INTERNET_DEFAULT_HTTPS_PORT : INTERNET_DEFAULT_HTTP_PORT, nullptr, nullptr, INTERNET_SERVICE_HTTP, 0, 0 );
	if ( !_connect ) {
		printf( "InternetConnect failed\n" );
		return;
	}

	auto dwFlags = secure ? INTERNET_FLAG_SECURE : 0x0 | INTERNET_FLAG_IGNORE_CERT_CN_INVALID | INTERNET_FLAG_IGNORE_CERT_DATE_INVALID;
	_request = HttpOpenRequestA( _connect, http_send_type_char [ httpsendtype ].c_str( ), _post.c_str( ), "HTTP/1.1", nullptr, nullptr, dwFlags, 0 );
	if ( !_request ) {
		printf( "HttpOpenRequest failed\n" );
		return;
	}
}

/*
*	send_data
*	  - store the inputted data
*/
void http::send_data( std::string send_data ) {
	_send_data = send_data;
}

/*
*	headers
*	  - store the headers in a vector and add to request headers for later use
*/
void http::headers( std::vector < std::string > headers ) {
	_headers = headers;
	for ( auto& header : _headers) {
		header.append( "\r\n" );
		HttpAddRequestHeadersA( _request, header.c_str( ), strlen( header.c_str( ) ), HTTP_ADDREQ_FLAG_REPLACE | HTTP_ADDREQ_FLAG_ADD );
	}
}

/*
*	get
*	  - get the data that you sent or return the data the server sent you
*/
std::string http::get( http_get_type httpgettype ) {
	unsigned long bytes_available, bytes_read;
	if ( httpgettype == http_get_type::response_headers ) {
		
	}
	else if ( httpgettype == http_get_type::response_body ) {
		while ( InternetQueryDataAvailable( _request, &bytes_available, 0, 0 ) ) {
			const auto message_body = ( byte* ) malloc( bytes_available + 1 );

			if ( !InternetReadFile( _request, message_body, bytes_available, &bytes_read ) ) {
				fprintf( stderr, "InternetReadFile failed, error = %d (0x%x)\n", GetLastError( ), GetLastError( ) );
				break;
			}

			if ( !bytes_read )
				break;	// End of File.

			const auto string = std::string( message_body, message_body + bytes_read );
			free( message_body );
			return string;
		}
	}
}

/*
*	send
*	  - send the information the the web page
*/
void http::send( ) {
	unsigned long status_code_length = sizeof( _status_code );

	HttpSendRequestA( _request, nullptr, 0, ( LPVOID ) _send_data.c_str( ), strlen( _send_data.c_str( ) ) );
	HttpQueryInfoA( _request, HTTP_QUERY_STATUS_CODE | HTTP_QUERY_FLAG_NUMBER, &_status_code, &status_code_length, nullptr );
}

/*
*	cleanup
*	  - clean up internet handles unused memory, etc (only use when done)
*/
void http::cleanup( ) {
	InternetCloseHandle( _request );
	InternetCloseHandle( _connect );
	InternetCloseHandle( _internet );
}